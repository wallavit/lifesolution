$(document).ready(function() {	
	$(document).on('click', '#circuloDerecho', function(){
		cambiarUltimo();
	});

	$(document).on('click', '.textoContacto', function(){
		cambiarUltimo();
	});
	
	$(document).on('click', '#contactoDiv', function(){
		cambiarUltimo();
	});
});
function mostrarTerceraPagina() {
	$( "#circuloIzquierdo" ).removeClass("circSelec").addClass( "circNoSelec" );
	$( "#circuloCentro" ).removeClass("circSelec").addClass( "circNoSelec" );
	$( "#circuloDerecho" ).removeClass("circNoSelec").addClass( "circSelec" );	

	$( "#celular2" ).removeClass("hide").addClass( "show" );
	$( "#terceraPagina" ).removeClass("hide").addClass( "show" );

}
function ocultarTerceraPagina(argument) {
	$( "#celular2" ).removeClass("show").addClass( "hide" );
	$( "#terceraPagina" ).removeClass("show").addClass( "hide" );	
}

